#!/root/.nvm/versions/node/v7.9.0/bin/node

// Set vars and libs
const fs = require('fs');
const _ = require('lodash');

//Database
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Connect to Mongo
mongoose.connect('mongodb://localhost/photos');

// Setup Schema
const photoSchema = new Schema({
    tag: String,
    make: String,
    model: String,
    exposure: String,
    shutterSpeed: String,
    isoSpeed: String,
    focalLength: String
});

const Photo = mongoose.model('Photo', photoSchema);

const topTags = ["nature","macro","sky","portrait","blue","bw","usa","california","water","red","flower","night","green","light","people","sunset","beach","clouds","white","abigfave"];
const fields = ["make", "model", "exposure", "shutterSpeed", "isoSpeed", "focalLength"];

topTags.forEach((currentTag)=>{
    fields.forEach((currentField)=>{
        let currentFieldString = "$" + currentField;

        Photo.aggregate(
            [
                {
                    $match: {
                        tag: currentTag
                    }
                },
                {
                    $sortByCount: currentFieldString
                },
                {
                    $limit: 3
                },
                {
                    $project: {
                        _id: 0,
                        tag: "$_id",
                        make: currentFieldString,
                        count: 1
                    }
                },

            ],
            (err, results)=>{
                if(err){
                    throw err;
                }

                console.log(`Data for ${currentTag} and ${currentField}:`);
                results.forEach((data, i)=>{
                    console.log(`${data.tag} (${data.count})`);
                });
            }
        )
    })
});
