let _ = require( 'lodash' );
let fs = require( 'fs' );

//Vars
let tagFile = '';
let exifFile = '';
let exifFileContents = '';
let exifFileArray = [];
let tagFileContents = '';
let tagFileArray = [];
let currentObj = {};
let prevKey = '';

//Get all exif directories
fs.readdir( 'data/exif', ( err, exifDirs ) => {
    exifDirs.shift();

    //Get all tag directories
    fs.readdir( 'data/tags', ( err, tagsDirs ) => {
        tagsDirs.shift();

        //Loop through all exif directories
        exifDirs.forEach( ( exifDir ) => {
            //if ( exifDir === '0' ) {
                let currentExifDir = `data/exif/${exifDir}`;

                //Get exif directory files
                fs.readdir( currentExifDir, ( err, exifFiles ) => {
                    if ( err ) {
                        process.exit();
                    }


                    //Loop through all exif files
                    exifFiles.forEach( ( exifFile ) => {
                        //Check if there is an associated tags file
                        tagFile = `data/tags/${exifDir}/${exifFile}`;
                        exifFile = `data/exif/${exifDir}/${exifFile}`;

                        if ( fs.existsSync( tagFile ) ) {
                            //Cool, now we can start parsing the file into JSON
                            exifFileContents = fs.readFileSync( exifFile ).toString( 'utf-8' );
                            exifFileArray = exifFileContents.split( "\r\n" );

                            // Clear out the current obj
                            currentObj = {
                                trackingData: {
                                    exifFile: exifFile,
                                    tagsFile: tagFile
                                }
                            };
                            prevKey = '';
                            //Break out the exif into json
                            exifFileArray.forEach( ( data, i ) => {
                                if ( i % 2 ) {
                                    //Odd
                                    if(!prevKey.match(/^(Tag::|Picture Info|Proccessing Info|Color Balance|UnknownBlock|Canon File Info)/)){
                                        currentObj[ prevKey ] = data;
                                    }
                                } else {
                                    //Even
                                    prevKey = data.replace( /^\-/, '' );
                                }
                            } );

                            tagFileContents = fs.readFileSync( tagFile ).toString( 'utf-8' );
                            tagFileArray = _.compact(tagFileContents.split( "\r\n" ));

                            currentObj.tags = tagFileArray;

                            fs.appendFileSync(`data/output/${exifDir}.txt`, JSON.stringify(currentObj) + "\n", 'utf8');
                        } else {
                            console.log( 'tag file does not exist' );
                        }
                    } );
                } );
            // }
        } );
    } );
} );
