#!/root/.nvm/versions/node/v7.9.0/bin/node

// Set vars and libs
const fs = require('fs');
// const readline = require('readline');
// const devnull = require('dev-null');
const _ = require('lodash');
const LineReader = require('line-by-line');
const lr = new LineReader('data/photoData.json')

//Database
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let input = '';
let photoData = [];

// Set encoding
process.stdin.setEncoding('utf8');

// Connect to Mongo
mongoose.connect('mongodb://localhost/photos');

// Setup Schema
const photoSchema = new Schema({
    tag: String,
    make: String,
    model: String,
    exposure: String,
    shutterSpeed: String,
    isoSpeed: String,
    focalLength: String
});

const Photo = mongoose.model('Photo', photoSchema);

lr.on('line', (line)=>{
    processData(line);
});

lr.on('error', (err)=>{
    throw err;
});

lr.on('end', ()=>{
    console.log('Processing complete');
})


//Setup readline - This was for the original Hadoop process
// const rl = readline.createInterface({
//     input: process.stdin,
//     output: devnull
// });
//
// rl.on('line', (input)=>{
//     processData(input);
// })
//
// rl.on('end', ()=>{
//
// });

function processData(line){
    try{
        let photo = JSON.parse(line);

        if(photo.tags && photo.tags.length){
            if(
                _.get(photo, 'Make') &&
                _.get(photo, 'Model') &&
                _.get(photo, 'Exposure') &&
                _.get(photo, 'Shutter Speed') &&
                _.get(photo, 'ISO Speed') &&
                _.get(photo, 'Focal Length')
            ){
                photo.tags.forEach((tag)=>{
                    let savePhoto = {
                        'tag': tag,
                        'make': _.get(photo, 'Make'),
                        'model': _.get(photo, 'Model'),
                        'exposure': _.get(photo, 'Exposure'),
                        'shutterSpeed': _.get(photo, 'Shutter Speed'),
                        'isoSpeed': _.get(photo, 'ISO Speed'),
                        'focalLength': _.get(photo, 'Focal Length')
                    }
                    // console.log(JSON.stringify(savePhoto));
                    let newPhoto = new Photo(savePhoto);

                    newPhoto.save((err)=>{
                        if(err){
                            throw err;
                        }
                    })
                });
            }
        }
    }catch(e){
        // console.log(e, line);
        console.log('error', e);
    }
}
